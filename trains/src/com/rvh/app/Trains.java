package com.rvh.app;

import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;

/**
 * 
 * @author ronald vinaya
 * @email rvinayah@gmail.com
 * 
 */

public class Trains {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        ReadData readData = ReadData.getInstance();
        readData.readData();
        Vertex[] vertexs = readData.loadGraph();
        Graph graph = new Graph();
        graph.setVertices(vertexs);
        System.out.println(" ======== GRAPH ========== ");
        System.out.println(graph);
        
        System.out.println(" ======== loadDistanceOfRoute ========== ");
        List<Vertex[]> roads = readData.loadDistanceOfRoute();
        Iterator<Vertex[]> itDRoutes = roads.iterator();
        while (itDRoutes.hasNext()) {
            Vertex[] vRoads = itDRoutes.next();
            StringBuilder names = new StringBuilder();
            for(Vertex vi : vRoads){
                names.append(vi.getName());
                names.append("-");
            }
            double distance =graph.distanceOfRoute(vRoads, 0, 0.0);
            System.out.print("The distance of the route "+names+": ");
            System.out.println(distance == -1 ? "NO SUCH ROUTE" : distance );
        }
        
        System.out.println(" ========= loadNTripsStartingAtoBWithMax ========= ");
        List<Object[]> trips = readData.loadNTripsStartingAtoBWithMax();
        Iterator<Object[]> itTrips = trips.iterator();
        while (itTrips.hasNext()) {
            Object[] data = itTrips.next();
            Long maxStops = (Long)data[0];
            graph.numberOfTrips((Vertex)data[1], (Vertex)data[1], (Vertex)data[2], 
                                        0, maxStops.intValue(), ((Vertex)data[1]).getName());
            
        }
        
        System.out.println(" ========= numberOfTripsExactly ========= ");
        List<Object[]> tripsEx = readData.loadNTripsStartingAtoBExactlyMax();
        Iterator<Object[]> itTripsEx = tripsEx.iterator();
        while (itTripsEx.hasNext()) {
            Object[] data = itTripsEx.next();
            Long maxStops = (Long)data[0];
            graph.numberOfTripsExactly((Vertex)data[1], (Vertex)data[1], (Vertex)data[2], 
                                        0, maxStops.intValue(), ((Vertex)data[1]).getName());
        }
        
        System.out.println(" ========== loadLenghtShortestRoadAtoB ======== ");
        List<Vertex[]> roadsSLWithout0 = readData.loadLenghtShortestRoadAtoB();
        Iterator<Vertex[]> itRoadsSLWithout0 = roadsSLWithout0.iterator();
        while (itRoadsSLWithout0.hasNext()) {
            Object[] data = itRoadsSLWithout0.next();
            Vertex a = (Vertex)data[0];
            Vertex b = (Vertex)data[1];
            System.out.print("Shorted path from "+a.getName()+" to "+b.getName()+": ");
            graph.computeFromAtoB(a, b);
            System.out.println(graph.getMinDistancePath()+" > "+graph.getMinDistance());
            graph.resetValues();
        }
        
        System.out.println(" ========= loadNumberDiferentRoutesFromAtoBWithDistanceLessThan ========= ");
        List<Object[]> roadsShortLenghtLT = readData.loadNumberDiferentRoutesFromAtoBWithDistanceLessThan();
        Iterator<Object[]> itRoadsSLLT = roadsShortLenghtLT.iterator();
        while (itRoadsSLLT.hasNext()) {
            Object[] data = itRoadsSLLT.next();
            Vertex a = (Vertex)data[0];
            Vertex b = (Vertex)data[1];
            Long distance = (Long)data[2];
            graph.numberDifferentRoutesFromAtoB((Vertex)data[0], (Vertex)data[1], distance.intValue());
            Hashtable hashRoads = graph.getRoads();
            System.out.println("The number of different routes from "+a.getName()+
                           " to "+b.getName()+
                           " with a distance of less than "+distance+
                           " is: "+hashRoads.size());
        }
    }
}