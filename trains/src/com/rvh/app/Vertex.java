package com.rvh.app;

/**
 * 
 * @author ronald vinaya
 * @email rvinayah@gmail.com
 * 
 */
public class Vertex implements Comparable<Vertex> {
    private String name;
    private Edge[] adjacencies;
    private double minDistance = Double.POSITIVE_INFINITY;
    private Vertex previous;
    
    public Vertex(String argName) { 
    	name = argName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Edge[] getAdjacencies() {
        return adjacencies;
    }

    public void setAdjacencies(Edge[] adjacencies) {
        this.adjacencies = adjacencies;
    }

    public double getMinDistance() {
        return minDistance;
    }

    public void setMinDistance(double minDistance) {
        this.minDistance = minDistance;
    }

    public Vertex getPrevious() {
        return previous;
    }

    public void setPrevious(Vertex previous) {
        this.previous = previous;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        if(adjacencies != null)
            for(Edge e: adjacencies){
               sb.append(e);
            }
        sb.append("]");
        
        return "Vertex{" + "name=" + name + 
                           ", adjacencies=" + sb.toString() + 
                           ", minDistance=" + minDistance + 
                            ", previous=" + (previous != null?previous.getName():"null") + '}';
    }
    
    public double hasAdjacencies(Vertex vertex){
        double resDistance = -1;
        for(int i = 0; i < adjacencies.length; i++){
            if(vertex.name.compareTo(adjacencies[i].getTarget().name) == 0){
              resDistance = adjacencies[i].getWeight();
              break;
            }
        }
        return resDistance;
    }

    @Override
    public int compareTo(Vertex other) {
       return this.name.compareTo(other.name);
    }
}

