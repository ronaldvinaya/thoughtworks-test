package com.rvh.app;

/**
 * 
 * @author ronald vinaya
 * @email rvinayah@gmail.com
 * 
 */
public class Edge {
    
    private final Vertex target;
    private final double weight;
    
    public Edge(Vertex argTarget, double argWeight) { 
        target = argTarget;
        weight = argWeight; 
    }

    public Vertex getTarget() {
        return target;
    }

    public double getWeight() {
        return weight;
    }

    @Override
    public String toString() {
        return "{" + "target=" + target.getName() + ", weight=" + weight + "}, ";
    }
}