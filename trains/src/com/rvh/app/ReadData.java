package com.rvh.app;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
/**
 *
 * @author ronald vinaya
 * @email rvinayah@gmail.com
 *
 */

public class ReadData {
    
    private JSONParser parser = new JSONParser();
    private static ReadData readData;
    private JSONObject jsonObject;
    private Hashtable<String, Integer> hashT;
    private Vertex[] arrayVertexs;
    
    private ReadData(){ }
    
    public static ReadData getInstance (){
        if(readData == null)
            readData = new ReadData();
        return readData;
    }
    public void readData(){
        try{
            Object obj = parser.parse(new FileReader("resources/example-data.txt"));
            jsonObject =  (JSONObject) obj;
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    
    public Vertex[] loadGraph(){
        JSONObject graph = (JSONObject) jsonObject.get("graph");
        JSONArray vertexs = (JSONArray)graph.get("vertexs");
        arrayVertexs = new Vertex[vertexs.size()];
        Iterator<String> itVertexs = vertexs.iterator();
        int i = 0;
        hashT = new Hashtable<String, Integer>();
        while (itVertexs.hasNext()) {
            arrayVertexs[i] = new Vertex(itVertexs.next());
            hashT.put(arrayVertexs[i].getName(), i);
            i++;
        }
        
        JSONArray edges = (JSONArray)graph.get("edges");
        
        Iterator itEdges = edges.iterator();
        while (itEdges.hasNext()) {
            JSONObject objEdge = (JSONObject) itEdges.next();
            String nameIni = (String)objEdge.get("ini");
            JSONArray adjacencies = (JSONArray)objEdge.get("adjacencies");
            Iterator itAdj = adjacencies.iterator();
            
            Edge[] adjacenciesArray = new Edge[adjacencies.size()];
            int j=0;
            while (itAdj.hasNext()) {
                JSONObject objAdj = (JSONObject) itAdj.next();
                String nameAdj = (String)objAdj.get("end");
                Long weight = (Long) objAdj.get("weight");
                
                Integer indexAdj = hashT.get(nameAdj);
                Vertex vAdj = arrayVertexs[indexAdj];
                adjacenciesArray[j] = new Edge(vAdj, weight);
                j++;
            }
            
            Integer indexIni = hashT.get(nameIni);
            Vertex vIni = arrayVertexs[indexIni];
            vIni.setAdjacencies(adjacenciesArray);
            
        }
        return arrayVertexs;
    }
    
    public List<Vertex[]> loadDistanceOfRoute(){
        JSONArray dRoutes = (JSONArray) jsonObject.get("testDistanceRoutes");
        Iterator itDRoutes = dRoutes.iterator();
        List<Vertex[]> roads = new ArrayList<Vertex[]>();
        
        while (itDRoutes.hasNext()) {
            JSONObject objRoute = (JSONObject) itDRoutes.next();
            JSONArray rVertexs = (JSONArray) objRoute.get("road");
            Iterator itRVertexs = rVertexs.iterator();
            Vertex[] rVertexRoads = new Vertex[rVertexs.size()];
            int i = 0;
            while (itRVertexs.hasNext()) {
                String nameVertex = (String) itRVertexs.next();
                rVertexRoads[i] = arrayVertexs[hashT.get(nameVertex)];
                i++;
            }
            roads.add(rVertexRoads);
        }
        
        return roads;
    }
    
    public List<Object[]> loadNTripsStartingAtoBWithMax(){
        JSONArray arrayTrips = (JSONArray) jsonObject.get("testNTripsStartingAtoBWithMax");
        Iterator itTrips = arrayTrips.iterator();
        List<Object[]> trips = new ArrayList<Object[]>();
        
        while (itTrips.hasNext()) {
            JSONObject objRoute = (JSONObject) itTrips.next();
            Long maxStops = (Long) objRoute.get("maxStops");
            String initVertex = (String) objRoute.get("initVertex");
            String endVertex = (String) objRoute.get("endVertex");
            Object[] data = new Object[3];
            data[0] = maxStops;
            data[1] = arrayVertexs[hashT.get(initVertex)];
            data[2] = arrayVertexs[hashT.get(endVertex)];
            trips.add(data);
        }
        
        return trips;
    }
    
    public List<Object[]> loadNTripsStartingAtoBExactlyMax(){
        JSONArray arrayTrips = (JSONArray) jsonObject.get("testNTripsStartingAtoBExactlyMax");
        Iterator itTrips = arrayTrips.iterator();
        List<Object[]> trips = new ArrayList<Object[]>();
        
        while (itTrips.hasNext()) {
            JSONObject objRoute = (JSONObject) itTrips.next();
            Long maxStops = (Long) objRoute.get("maxStops");
            String initVertex = (String) objRoute.get("initVertex");
            String endVertex = (String) objRoute.get("endVertex");
            Object[] data = new Object[3];
            data[0] = maxStops;
            data[1] = arrayVertexs[hashT.get(initVertex)];
            data[2] = arrayVertexs[hashT.get(endVertex)];
            trips.add(data);
        }
        
        return trips;
    }
    
    public List<Vertex[]> loadLenghtShortestRoadAtoB(){
        JSONArray arrayRoads = (JSONArray) jsonObject.get("testLenghtShortestRoadAtoB");
        Iterator itRoads = arrayRoads.iterator();
        List<Vertex[]> roads = new ArrayList<Vertex[]>();
        
        while (itRoads.hasNext()) {
            JSONObject objRoute = (JSONObject) itRoads.next();
            String initVertex = (String) objRoute.get("initVertex");
            String endVertex = (String) objRoute.get("endVertex");
            Vertex[] data = new Vertex[2];
            data[0] = arrayVertexs[hashT.get(initVertex)];
            data[1] = arrayVertexs[hashT.get(endVertex)];
            roads.add(data);
        }
        return roads;
    }
    
    public List<Object[]> loadNumberDiferentRoutesFromAtoBWithDistanceLessThan(){
        JSONArray arrayRoads = (JSONArray) jsonObject.get("testNumberDiferentRoutesFromAtoBWithDistanceLessThan");
        Iterator itRoads = arrayRoads.iterator();
        List<Object[]> roads = new ArrayList<Object[]>();
        
        while (itRoads.hasNext()) {
            JSONObject objRoute = (JSONObject) itRoads.next();
            String initVertex = (String) objRoute.get("initVertex");
            String endVertex = (String) objRoute.get("endVertex");
            Long distance =(Long) objRoute.get("distance");
            Object[] data = new Object[3];
            data[0] = arrayVertexs[hashT.get(initVertex)];
            data[1] = arrayVertexs[hashT.get(endVertex)];
            data[2] = distance; 
            roads.add(data);
        }
        return roads;
    }
    
    
}
