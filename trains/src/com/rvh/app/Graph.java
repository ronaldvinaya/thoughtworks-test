package com.rvh.app;

import java.util.Hashtable;

/**
 *
 * @author ronaldvinaya
 * @email rvinayah@gmail.com
 * 
 */
public class Graph {
    private int numberTrips = 0;
    private Vertex[] vertices;
    private double minDistance = 10000;
    private String minDistancePath = "";
    
    private Hashtable roads = new Hashtable<String,Double>();

    public int getNumberTrips() {
        return numberTrips;
    }

    public void setNumberTrips(int numberTrips) {
        this.numberTrips = numberTrips;
    }

    public Hashtable getRoads() {
        return roads;
    }

    public void setRoads(Hashtable roads) {
        this.roads = roads;
    }
    
    public double getMinDistance() {
        return minDistance;
    }

    public void setMinDistance(double minDistance) {
        this.minDistance = minDistance;
    }

    public String getMinDistancePath() {
        return minDistancePath;
    }

    public void setMinDistancePath(String minDistancePath) {
        this.minDistancePath = minDistancePath;
    }
    
    public void setVertices(Vertex[] vertices){
        this.vertices = vertices;
    }
    
    public Vertex[] getVertices(){
        return this.vertices;
    }
    
    /*
     * Before to get the shortest Path from A to B it is necesary to have 
     * all the vertices that the graph had it.
     * this method recived the index to reference to vertices
     * 
     */
    public void computeFromAtoB(int ini, int end){
        StringBuilder sb = new StringBuilder();
        for(Vertex v: vertices){
            sb.append(v.getName());
            sb.append("-");
        }
        
        getShortestPathFromAtoB(vertices[ini], vertices[end], sb.toString(),
                                vertices[ini].getName(), 0.0, true);
    }
    
    /*
     * Before to get the shortest Path from A to B it is necesary to have 
     * all the vertices that the graph had it.
     * this method recived the two object vertices
     * 
     */
    public void computeFromAtoB(Vertex ini, Vertex end){
        StringBuilder sb = new StringBuilder();
        for(Vertex v: vertices){
            sb.append(v.getName());
            sb.append("-");
        }
        
        getShortestPathFromAtoB(ini, end, sb.toString(), ini.getName(), 0.0, true);
    }
    
    /*
     * Get the shortest Path from A to B
     */
    public void getShortestPathFromAtoB(Vertex vPivot, Vertex vEnd,String allVertex,
                                             String currentPath, double distance, boolean isFirst){
        
        if(isFirst) {
            for(Edge e: vPivot.getAdjacencies()){
                getShortestPathFromAtoB(e.getTarget(), vEnd, allVertex,
                                              currentPath+"-"+e.getTarget().getName(),
                                              distance + e.getWeight(), false);
            }
        } else if(vPivot.compareTo(vEnd) != 0 )
            for(Edge e: vPivot.getAdjacencies())
                if(!currentPath.contains(e.getTarget().getName()) && e.getTarget().compareTo(vEnd) != 0){
                    getShortestPathFromAtoB(e.getTarget(), vEnd, allVertex,
                                                  currentPath+"-"+e.getTarget().getName(),
                                                  distance + e.getWeight(), false);
                }else if( e.getTarget().compareTo(vEnd) == 0 ){
                    distance = distance + e.getWeight();
                    if(distance < this.minDistance ){
                        this.minDistance = distance;
                        this.minDistancePath = currentPath+"-"+e.getTarget().getName();
                    }
                }
                
    }
    
    /*
     * Reset all the values
     */
    public void resetValues(){
        for (Vertex v : vertices) {
             v.setMinDistance(Double.POSITIVE_INFINITY);
             v.setPrevious(null);
        }
        this.minDistance = 10000;
        this.minDistancePath = "";
    }
    
    /*
     * Caculate the exactly number of trips from vertex A to B 
     */
    public void numberOfTripsExactly(Vertex vPivot, Vertex vIni, Vertex vEnd, 
                                        int level, int numberStops, String res){
        if(level < numberStops){
            if(vIni.compareTo(vPivot)==0 && level != 0 && level == numberStops){
                System.out.println(res);
                numberTrips++;
            }else{
                for(Edge e: vPivot.getAdjacencies()){
                    numberOfTripsExactly( e.getTarget(), vIni, vEnd, level + 1, numberStops,
                                   res + "-"+e.getTarget().getName());
                }
            }
        } else {
            if(vEnd.compareTo(vPivot) == 0 && level == numberStops){
                System.out.println(res);
                numberTrips++;
            }
        }
    }
    
    /*
     * Caculate number of trips from vertex A to B  with a limit
     */
    public void numberOfTrips(Vertex vPivot, Vertex vIni, Vertex vEnd, 
                              int level, int numberStops, String res){
        if(level < numberStops){
            if(vIni.compareTo(vPivot) == 0 && level != 0){
                System.out.println(res);
                numberTrips++;
            }else{
                if(vPivot.compareTo(vEnd) == 0 && level != 0){
                    System.out.println(res);
                    numberTrips++;
                    for(Edge e: vPivot.getAdjacencies()){
                        numberOfTrips( e.getTarget(), vIni, vEnd, level + 1, numberStops,
                                       res + "-"+e.getTarget().getName());
                    }
                } else {
                    for(Edge e: vPivot.getAdjacencies()){
                        numberOfTrips( e.getTarget(), vIni, vEnd, level + 1, numberStops,
                                       res + "-"+e.getTarget().getName());
                    }
                }
            }
        } else {
            if(vEnd.compareTo(vPivot) == 0){
                System.out.println(res);
                numberTrips++;
            }
        }
    }
    
    /*
     *  Method to calculate the total distance from  A - B - C, if no such route exists,
     *  the result is -1
     */
    public double distanceOfRoute(Vertex[] vertices, int index, double distance) {
        if(index < vertices.length-1){
            double dist = vertices[index].hasAdjacencies(vertices[index+1]);
            if(dist > -1)
                return distanceOfRoute(vertices, index + 1, distance+dist);
            else
                return dist;
        }
    	return distance;
    }
    
    /*
     * For display number of different routes from A to B first it is necessary 
     * to setup initial values
     */
    public void numberDifferentRoutesFromAtoB(int ini, int end, int maxDistance){
        numberDifferentRoutesFromAtoB(vertices[ini], vertices[ini], vertices[end], 
                                      maxDistance, 0.0, 0, vertices[ini].getName());
    }
    
    /*
     * For display number of different routes from A to B first it is necessary 
     * to setup initial values
     */
    public void numberDifferentRoutesFromAtoB(Vertex ini, Vertex end, int maxDistance){
        numberDifferentRoutesFromAtoB(ini, ini, end, maxDistance, 0.0, 0, ini.getName());
    }
    
    /*
     *  Display number of different routes from A to B
     */
    public void numberDifferentRoutesFromAtoB(Vertex vPivot, Vertex vIni, Vertex vEnd, 
                                              int maxDistance, double distance,
                                              int level, String res){
        
        if(distance < maxDistance){
            if(vIni.compareTo(vPivot) == 0 && level != 0){
                roads.put(res,distance);
                for(Edge e: vPivot.getAdjacencies()){
                        numberDifferentRoutesFromAtoB( e.getTarget(), vIni, vEnd, maxDistance,
                                    distance + e.getWeight(), level + 1,
                                       res + "-"+e.getTarget().getName());
                    }
            }else{
                if(vPivot.compareTo(vEnd) == 0 && level != 0){
                    roads.put(res,distance);
                    for(Edge e: vPivot.getAdjacencies()){
                        numberDifferentRoutesFromAtoB( e.getTarget(), vIni, vEnd, maxDistance,
                                    distance + e.getWeight(), level + 1,
                                       res + "-"+e.getTarget().getName());
                    }
                } else {
                    for(Edge e: vPivot.getAdjacencies()){
                        numberDifferentRoutesFromAtoB( e.getTarget(), vIni, vEnd, maxDistance,
                                    distance + e.getWeight(), level + 1, res + "-"+e.getTarget().getName());
                    }
                }
            }
        } else if(vEnd.compareTo(vPivot) == 0 && distance < maxDistance)
            roads.put(res,distance);
        
    }
    
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (Vertex v : getVertices()) {
            sb.append(v);
            sb.append("\n");
        }
        return sb.toString();
    }
}