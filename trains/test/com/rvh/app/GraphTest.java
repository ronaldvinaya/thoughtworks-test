package com.rvh.app;

import java.util.Hashtable;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * 
 * @author ronald vinaya
 * @email rvinayah@gmail.com
 * 
 */
public class GraphTest {
    
    private Graph graph;
    private Vertex vA;
    private Vertex vB;
    private Vertex vC;
    private Vertex vD;
    private Vertex vE;
    
    public GraphTest() {
        graph = new Graph();
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        vA = new Vertex("A");
        vB = new Vertex("B");
        vC = new Vertex("C");
        vD = new Vertex("D");
        vE = new Vertex("E");

        vA.setAdjacencies(new Edge[]{ new Edge(vB, 5), new Edge(vD, 5), new Edge(vE, 7) });
        vB.setAdjacencies(new Edge[]{ new Edge(vC, 4) });
        vC.setAdjacencies(new Edge[]{ new Edge(vD, 8), new Edge(vE, 2) });
        vD.setAdjacencies(new Edge[]{ new Edge(vC, 8), new Edge(vE, 6) });
        vE.setAdjacencies(new Edge[]{ new Edge(vB, 3) });
        Vertex[] vertices =  new Vertex[5];
        vertices[0] = vA;
        vertices[1] = vB;
        vertices[2] = vC;
        vertices[3] = vD;
        vertices[4] = vE;
        graph.setVertices(vertices);
    }
    
    @After
    public void tearDown() { }
    
    /*
     *  1. The distance of the route A-B-C.
     *  2. The distance of the route A-D.
     *  3. The distance of the route A-D-C.
     *  4. The distance of the route A-E-B-C-D.
     *  5. The distance of the route A-E-D.
     */
    @Test
    public void testDistanceOfRoute(){
        System.out.println("=== DistanceOfRoute ===");
        Vertex[] vertDist_1 = { vA, vB, vC};
        double distance1 = graph.distanceOfRoute(vertDist_1, 0, 0.0);
        System.out.print("The distance of the route A-B-C: ");
        System.out.println(distance1 == -1 ? "NO SUCH ROUTE" : distance1 );
        
        Vertex[] vertDist_2 = { vA, vD};
        double distance2 = graph.distanceOfRoute(vertDist_2, 0, 0.0);
        System.out.print("The distance of the route A-D: ");
        System.out.println(distance2 == -1?"NO SUCH ROUTE":distance2);
        
        Vertex[] vertDist_3 = { vA, vD, vC};
        double distance3 = graph.distanceOfRoute(vertDist_3, 0, 0.0);
        System.out.print("The distance of the route A-D-C: ");
        System.out.println(distance3 == -1?"NO SUCH ROUTE":distance3);
                
        Vertex[] vertDist_4 = { vA, vE, vB, vC, vD};
        double distance4 = graph.distanceOfRoute(vertDist_4, 0, 0.0);
        System.out.print("The distance of the route A-E-B-C-D: ");
        System.out.println(distance4 == -1?"NO SUCH ROUTE":distance4);
        
        Vertex[] vertDist_5 = { vA, vE, vD};
        double distance5 = graph.distanceOfRoute(vertDist_5, 0, 0.0);
        System.out.print("The distance of the route A-E-D: ");
        System.out.println(distance5 == -1?"NO SUCH ROUTE":distance4);
        System.out.println();
    }
    
    /*
     * 6. The number of trips starting at C and ending at C with a maximum of 3 stops.  
     *    In the sample data below, there are two such trips: C-D-C (2 stops). 
     *    and C-E-B-C (3 stops).
     */
    @Test
    public void testNumberOfTrips(){
        System.out.println("=== NumberOfTrips ===");
        graph.setNumberTrips(0);
        graph.numberOfTrips(vC, vC, vC, 0, 3, vC.getName());
        System.out.println("Number of trips from "+
                           vC.getName()+" to "+vC.getName()+": "+graph.getNumberTrips());
        System.out.println();

    }
    
    /*
     * 7. The number of trips starting at A and ending at C with exactly 4 stops. 
     *    In the sample data below, there are three such trips: A to C (via B,C,D); 
     *    A to C (via D,C,D); and A to C (via D,E,B).
     */
    
    @Test
    public void testNumberOfTripsExactly(){
        System.out.println("=== NumberOfTripsExactly ===");
        graph.setNumberTrips(0);
        graph.numberOfTripsExactly(vA, vA, vC, 0, 4, vA.getName());
        System.out.println("Number of trips Exactly from "+
                           vA.getName()+" to "+vC.getName()+": "+graph.getNumberTrips());
        System.out.println();
    }
    
    /**
     * 
     * 8. The length of the shortest route (in terms of distance to travel) from A to C.
     * 9. The length of the shortest route (in terms of distance to travel) from B to B.
     * 
     */
    @Test
    public void testGetShortestPathFromTo(){
        System.out.println("=== GetShortestPathFromAtoB ===");
        System.out.println("Shorted path from B to B: ");
        graph.computeFromAtoB(1, 1);
        System.out.println(graph.getMinDistancePath()+" > "+graph.getMinDistance());
        System.out.println("Shorted path from A to C: ");
        graph.resetValues();
        
        graph.computeFromAtoB(0, 2);
        System.out.println(graph.getMinDistancePath()+" > "+graph.getMinDistance());
        System.out.println();
    }
    
    /**
     * 
     * 10.The number of different routes from C to C with a distance of less than 30. 
     * In the sample data, the trips are: CDC, CEBC, CEBCDC, CDCEBC, CDEBC, CEBCEBC, CEBCEBCEBC.
     * 
     */
    @Test
    public void testNumberDifferentRoutesFromAtoB(){
        System.out.println("=== NumberDifferentRoutesFromAtoB ===");
        int distance = 30;
        graph.numberDifferentRoutesFromAtoB(2, 2, distance);
        Hashtable roads = graph.getRoads();
        System.out.println("The number of different routes from "+vA.getName()+
                           " to "+vB.getName()+
                           " with a distance of less than "+distance+
                           " is: "+roads.size());
    }
}