=== DistanceOfRoute ===
The distance of the route A-B-C: 9.0
The distance of the route A-D: 5.0
The distance of the route A-D-C: 13.0
The distance of the route A-E-B-C-D: 22.0
The distance of the route A-E-D: NO SUCH ROUTE

=== NumberOfTrips ===
C-D-C
C-E-B-C
Number of trips from C to C: 2

=== NumberOfTripsExactly ===
A-B-C-D-C
A-D-C-D-C
A-D-E-B-C
Number of trips Exactly from A to C: 3

=== GetShortestPathFromAtoB ===
Shorted path from B to B: 
B-C-E-B > 9.0
Shorted path from A to C: 
A-B-C > 9.0

=== NumberDifferentRoutesFromAtoB ===
The number of different routes from A to B with a distance of less than 30 is: 7
